Ideas to Improve this Game:


1. Add a win/lose statement at the conclusion of the match
2. Account for the user inputting something other than the given options (ie, what happens if the user types '78' or 'magical wand'  instead of '1', '2', or '3'?)
3. Add an option to either replay or quit the game.


These can be tackled in any order you like! :)